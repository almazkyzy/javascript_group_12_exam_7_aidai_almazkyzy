import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../shared/item-model";

@Component({
  selector: 'app-items',
  templateUrl:'./items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent {
  @Input() items: Item[]=[];
  @Output() itemAdded = new EventEmitter();

  addItemToOrderDetails(item: Item[]) {
    this.itemAdded.emit(item);
  }
}
