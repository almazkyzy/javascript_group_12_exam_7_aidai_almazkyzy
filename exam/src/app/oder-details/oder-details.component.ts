import {Component, EventEmitter, Input, Output,} from '@angular/core';
import {Item} from "../shared/item-model";

@Component({
  selector: 'app-oder-details',
  templateUrl: './oder-details.component.html',
  styleUrls: ['./oder-details.component.css']
})
export class OderDetailsComponent {
  @Input() items: Item[] = [];
  @Output() itemRemoved = new EventEmitter();
  calcTotal() {
    return this.items.reduce((acc, item) => acc+= item.price ,0)
  };
  removeItem(item: any) {
    this.itemRemoved.emit(item)
  }

}
