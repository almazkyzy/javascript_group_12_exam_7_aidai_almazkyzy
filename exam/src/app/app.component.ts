import { Component } from '@angular/core';
import {Item} from "./shared/item-model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title ='exam';
  items = [
    {name: 'Burger', price: 99},
    {name: 'CheeseBurger', price: 99},
    {name: 'Fries', price: 99},
    {name: 'Burger', price: 99},
    {name: 'CheeseBurger', price: 99},
    {name: 'Fries', price: 99},
  ];
  orderDetails = [];

  addItemToOrderDetails(item: Item[]) {
    const itemExistInOrderDetails = this.orderDetails.find(({name}) => name === item.name);
    if (!itemExistInOrderDetails) {
      this.orderDetails.push({...item, "num":1});
      return;
    }
    itemExistInOrderDetails.num += 1;
  }
  removeItem(item: Item[]) {
    this.orderDetails = this.orderDetails.filter(({name}) => name !== item.name)
  }
}
