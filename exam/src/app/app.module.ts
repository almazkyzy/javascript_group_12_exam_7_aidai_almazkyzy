import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import { ItemsComponent } from './items/items.component';
import { OderDetailsComponent } from './oder-details/oder-details.component';
import { ItemComponent } from './item/item.component';
import { OrderDetailsItemComponent } from './order-details-item/order-details-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    OderDetailsComponent,
    ItemComponent,
    OrderDetailsItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
