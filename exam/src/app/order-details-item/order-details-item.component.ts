import {Component, EventEmitter, Input, Output,} from '@angular/core';
import {Item} from "../shared/item-model";

@Component({
  selector: 'app-order-details-item',
  templateUrl: './order-details-item.component.html',
  styleUrls: ['./order-details-item.component.css']
})
export class OrderDetailsItemComponent {
  @Input() item: any;
  @Output() itemRemoved = new EventEmitter();
  modelChanged(item: Item []) {
    if (this.item.num === 0) {
      this.itemRemoved.emit(this.item)
    }
  }
}
